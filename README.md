Vim files for remote servers.

To get started:

```
# first see what options vim has been compiled with
vim --version

# now get the files
git clone https://gitlab.com/ahillio/remote-vim.git .vim && ln -s .vim/vimrc .vimrc
```
